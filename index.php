<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package scm_buffer
 */

get_header();
?>
<?php
if (is_home() && !is_paged()) :
 $about_query = new WP_Query( array(
   'post_type'   => 'page',
   'post_status' => 'publish',
   'pagename'    => 'about'
 ) );
 if ($about_query->have_posts()):
   while ($about_query->have_posts()): $about_query->the_post(); ?>
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<?php endif; ?>
     <div class="about">
			<a href="<?php the_permalink(); ?>">
        <div class="about-img">
          <?php the_post_thumbnail(); ?>
        </div>
			</a>
      <div class="about-description">
         <h2 class="about-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
         <p class="about-summary">
           <?php echo mb_substr(get_the_excerpt(),0,250); ?>
         </p>
         <a href="<?php the_permalink();?>" class="read-more">もっと見る</a>
      </div>
      </div>
     <?php
   endwhile;
 endif;
 wp_reset_postdata();
endif;
?>
<div id="primary" class="content-area home">
	<main id="main" class="site-main" role="main">
		<div class="top-page">
			<?php
			if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

			/*
			 * Include the Post-Format-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
			 */
			get_template_part( 'template-parts/content', get_post_format() );

			endwhile;
			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
		</div>
		<?php the_posts_pagination(); ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
