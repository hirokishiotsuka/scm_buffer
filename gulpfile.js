'use strict';
/*------------------------------------------------------------------------------
 * 1. DEPENDENCIES
------------------------------------------------------------------------------*/
var gulp        = require('gulp'),
    $           = require('gulp-load-plugins')({ pattern: ['gulp-*', 'gulp.*'] }),
    runSequence = require('run-sequence'),
    rimraf      = require('rimraf'),
    pngquant    = require('imagemin-pngquant'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload
;

/*------------------------------------------------------------------------------
 * 2. FILE DESTINATIONS (RELATIVE TO ASSSETS FOLDER)
------------------------------------------------------------------------------*/
// @param Choose css framework between bourbon and scss
// @param false or virtual host name of local machine such as . Set false to browser-sync start as server mode.
// @param false or Subdomains which must be between 4 and 20 alphanumeric characters.
var conf = {
	src   :			"./src",
	dist  :			"./assets",
	url   :			"localhost:8888",
	imgExt:			"png,jpg,jpeg,gif,svg"
};

var plumberErrorHandler = {
	errorHandler: $.notify.onError({
		title: 'Gulp',
		message: 'Error: <%= error.message %>'
	})
};

/*------------------------------------------------------------------------------
 * 3. Gulp Tasks
------------------------------------------------------------------------------*/
gulp.task('default', ['build', 'watch', 'browser-sync']);

gulp.task('clean', function(cb) {
	rimraf(conf.dist, cb);
});

gulp.task('build', ['clean'], function(cb) {
	runSequence(['image', 'js', 'sass', 'css', 'php'], cb);
});

gulp.task('browser-sync', function () {
	browserSync({
		proxy: conf.url
	});
});

gulp.task('watch', function() {
	gulp.watch(conf.src + '/images/*.{' + conf.imgExt + '}', ['image']);
	gulp.watch(conf.src + '/scss/**/*.scss', ['sass']);
	gulp.watch(['./inc/slider/css/*.css', './layouts/*.css'], ['css']);
	gulp.watch([conf.src + '/js/*.js', './js/*.js'], ['js']);
	gulp.watch(['./*.php', './template-parts/*.php', './inc/**/*.php'], ['php']);
});

gulp.task('php', function() {
	gulp.src(['./*.php', './template-parts/*.php', './inc/**/*.php'])
		.pipe($.plumber(plumberErrorHandler))
		.pipe(reload({ stream: true }));
	}
);

gulp.task('image', function() {
	var compressOption = {
		optimizationLevel: 7,
		progressive: true,
		interlaced: true,
		use: [
			pngquant({
				quality: 65-80,
				speed: 1
			})
		]
	};
	return gulp.src(conf.src +'/images/*.{' + conf.imgExt + '}')
		.pipe($.changed(conf.src + "/images/"))
		.pipe($.plumber(plumberErrorHandler))
		.pipe($.imagemin(compressOption))
		.pipe(gulp.dest(conf.src + "/images/"));
});

gulp.task('sass', function () {
	gulp.src(conf.src + '/scss/*.scss')
		.pipe($.plumber(plumberErrorHandler))
		.pipe($.sourcemaps.init())
		.pipe($.sass())
		.pipe($.autoprefixer())
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('./'))
		.pipe(reload({ stream: true }));
});

gulp.task('css', function () {
	gulp.src(['./inc/slider/css/*.css', './layouts/*.css'])
  	.pipe($.plumber(plumberErrorHandler))
  	.pipe(reload({ stream: true }));
});

gulp.task('js', function () {
	gulp.src([conf.src + '/js/*.js'])
  	.pipe($.plumber(plumberErrorHandler))
  	.pipe($.concat('bundle.js'))
  	.pipe($.uglify({ preserveComments: 'license' }))
  	.pipe($.rename({ suffix: '.min' }))
  	.pipe(gulp.dest(conf.dist + '/script/'))
  	.pipe(reload({ stream: true }));
});
