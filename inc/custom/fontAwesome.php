<?php
/*
 * Enable support for Font-Awesome Information.
 */
function save_custom_fontawesome_fields( $post_id ) {
	if(isset($_POST['fa-home']) && $_POST['fa-home'] !== '') {
		update_post_meta($post_id, 'fa-home', $_POST['fa-home'] );
	} else {
		delete_post_meta($post_id, 'fa-home');
	}
	if(isset($_POST['fa-angle-double-up']) && $_POST['fa-angle-double-up'] !== '') {
		update_post_meta($post_id, 'fa-angle-double-up', $_POST['fa-angle-double-up'] );
	} else {
		delete_post_meta($post_id, 'fa-angle-double-up');
	}
	if(isset($_POST['fa-folder-open']) && $_POST['fa-folder-open'] !== '') {
		update_post_meta($post_id, 'fa-folder-open', $_POST['fa-folder-open'] );
	} else {
		delete_post_meta($post_id, 'fa-folder-open');
	}
	if(isset($_POST['fa-tags']) && $_POST['fa-tags'] !== '') {
		update_post_meta($post_id, 'fa-tags', $_POST['fa-tags'] );
	} else {
		delete_post_meta($post_id, 'fa-tags');
	}
}

add_action('admin_menu', 'add_custom_fields');
add_action('save_post', 'save_custom_fontawesome_fields');

//Add SEO meta box at dashboard front pages
function add_fontawesome_ajaxurl() {
?>
	<script>
		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
	</script>
<?php
}
add_action( 'wp_head', 'add_fontawesome_ajaxurl', 1 );

function fontawesome_add_custom_box() {
	add_meta_box('fontawesome_meta_id', 'FontAwesomeアイコン変更用', 'fontawesome_inner_custom_box', 'dashboard', 'normal', 'high', 'fontawesome_save_postdata');
}

function fontawesome_inner_custom_box() {
	$fa_home = '';
	$fa_angle_double_up = '';
	$fa_folder_open = '';
	$fa_tags = '';
	if (isset($post) && $post !== '') {
		$fa_home = get_post_meta($post->ID, 'fa-home', true);
		$fa_angle_double_up = get_post_meta($post->ID, 'fa-angle-double-up', true);
		$fa_folder_open = get_post_meta($post->ID, 'fa-folder-open', true);
		$fa_tags = get_post_meta($post->ID, 'fa-tags', true);
	}
	$allpost = new WP_Query( array(
		'posts_per_page' => 1,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC'
	) );
	if ($allpost->have_posts()) :
		while ( $allpost->have_posts() ) :
			$allpost->the_post();
			$global_custom = get_post_custom();
			$fa_home = array_key_exists('fa-home', $global_custom) ? $global_custom['fa-home'][0] : '';
			$fa_angle_double_up = array_key_exists('fa-angle-double-up', $global_custom) ? $global_custom['fa-angle-double-up'][0] : '';
			$fa_folder_open = array_key_exists('fa-folder-open', $global_custom) ? $global_custom['fa-folder-open'][0] : '';
			$fa_tags = array_key_exists('fa-tags', $global_custom) ? $global_custom['fa-tags'][0] : '';
		endwhile;
	endif;
	$direc = get_bloginfo('template_directory');
	wp_enqueue_script('font_awesome_script', $direc . '/js/font-awesome.js');
	wp_enqueue_style('font_awesome_style', $direc . '/layouts/font-awesome.css', array(), '20130115', true );
	echo '<pre style="background-color: rgb(238, 238, 238);padding: 5px;box-sizing: border-box;box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.09) inset;">';
	echo 'fa-home: <i class="fa fa-home"></i><br />';
	echo ' &lt;i class="fa ※※※①※※※" aria-hidden="true"&gt;<br />';
	echo 'fa-angle-double-up: <i class="fa fa-angle-double-up"></i><br />';
	echo ' &lt;i class="fa ※※※②※※※" aria-hidden="true"&gt;<br />';
	echo 'fa-folder-open: <i class="fa fa-folder-open"></i><br />';
	echo ' &lt;i class="fa ※※※③※※※" aria-hidden="true"&gt;<br />';
	echo 'fa-tags: <i class="fa fa-tags"></i><br />';
	echo ' &lt;i class="fa ※※※④※※※" aria-hidden="true"&gt;<br />';
	echo '</pre>';
	echo '<hr />';
	echo '<h4>※Font Awesomeサイト(https://fortawesome.github.io/Font-Awesome/icons/)から「fa-」で始まるクラス名をここに入力してください。</h4>';
	echo '<p>①パンくずリストHOMEアイコン（fa-home）</p>';
	echo '<input id="id-fa-home" type="text" name="fa-home" value="'.esc_html($fa_home).'" placeholder="fa-home" maxlength="160" size="60" style="max-width: 100%;" /></p>';
	echo '<p>②ページトップ矢印アイコン（fa-angle-double-up）</p>';
	echo '<input id="id-fa-angle-double-up" type="text" name="fa-angle-double-up" value="'.esc_html($fa_angle_double_up).'" placeholder="fa-angle-double-up" maxlength="160" size="60" style="max-width: 100%;" /></p>';
	echo '<p>③カテゴリーアイコン（fa-folder-open）</p>';
	echo '<input id="id-fa-folder-open" type="text" name="fa-folder-open" value="'.esc_html($fa_folder_open).'" placeholder="fa-folder-open" maxlength="160" size="60" style="max-width: 100%;" /></p>';
	echo '<p>④タグアイコン（fa-tags）</p>';
	echo '<input id="id-fa-tags" type="text" name="fa-tags" value="'.esc_html($fa_tags).'" placeholder="fa-tags" maxlength="160" size="60" style="max-width: 100%;" /></p>';
	echo '<input id="fa-submit" class="button button-secondary button-large button-id" type="submit" value="保存" name="publish" />';
}

function fontawesome_save_postdata($post_id) {
	$allpost = new WP_Query( array(
		'posts_per_page' => 1,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC'
	) );
	if ($allpost->have_posts()) :
		$counter = 0;
		while ( $allpost->have_posts() ) :
			$allpost->the_post();
			$fontawesome_postID = $allpost->posts[$counter]->ID;
			add_post_meta($fontawesome_postID, 'fa-home', '0', true);
			add_post_meta($fontawesome_postID, 'fa-angle-double-up', '0', true);
			add_post_meta($fontawesome_postID, 'fa-folder-open', '0', true);
			add_post_meta($fontawesome_postID, 'fa-tags', '0', true);
			if(isset($_POST['ajax_fa-home']) && $_POST['ajax_fa-home'] !== '') {
				update_post_meta($fontawesome_postID, 'fa-home', $_POST['ajax_fa-home'] );
			} else {
				delete_post_meta($fontawesome_postID, 'fa-home');
			}
			if(isset($_POST['ajax_fa-angle-double-up']) && $_POST['ajax_fa-angle-double-up'] !== '') {
				update_post_meta($fontawesome_postID, 'fa-angle-double-up', $_POST['ajax_fa-angle-double-up'] );
			} else {
				delete_post_meta($fontawesome_postID, 'fa-angle-double-up');
			}
			if(isset($_POST['ajax_fa-folder-open']) && $_POST['ajax_fa-folder-open'] !== '') {
				update_post_meta($fontawesome_postID, 'fa-folder-open', $_POST['ajax_fa-folder-open'] );
			} else {
				delete_post_meta($fontawesome_postID, 'fa-folder-open');
			}
			if(isset($_POST['ajax_fa-tags']) && $_POST['ajax_fa-tags'] !== '') {
				update_post_meta($fontawesome_postID, 'fa-tags', $_POST['ajax_fa-tags'] );
			} else {
				delete_post_meta($fontawesome_postID, 'fa-tags');
			}
			$counter++;
		endwhile;
	endif;
}

add_action('admin_menu', 'fontawesome_add_custom_box');
add_action('wp_ajax_fontawesome_save_postdata', 'fontawesome_save_postdata');
add_action('wp_ajax_nopriv_fontawesome_save_postdata', 'fontawesome_save_postdata' );
