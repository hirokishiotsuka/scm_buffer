<?php

/** Register default page **/
function insert_page() {

  $my_post = array(
    'post_name'     => 'about',
    'post_title'    => 'このサイトについて',
    'post_content'  => 'これはデフォルトで最初に挿入される固定ページです。サイトの説明を記載する等の用途にお使いください。スラッグは「about」固定ですが、ページのタイトルは自由に変更可能です。本固定ページのサムネイルとして設定した画像が本ウィジェットの背景画像になります。',
    'post_status'   => 'publish',
    'post_type'     => 'page'
  );

  // Check duplication
  $about_query = new WP_Query( array(
    'post_type' => 'page',
    'pagename' => 'about'
  ));
  $about_wild_query = new WP_Query( array(
    'post_type' => 'page',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1,
    'nopaging' => true
  ));

  if ($about_query->have_posts()) {
    return;
  } else {
    if ($about_wild_query->have_posts()) :
      $wild_count = 0;
      $total_count = $about_wild_query->found_posts;
      for ($wild_count = 0; $wild_count < $total_count; $wild_count++) {
        $slug = $about_wild_query->posts[$wild_count]->post_name;
        $word_to_compare = substr($slug, 0, mb_strlen('about'));
        if ($word_to_compare === 'about') {
          return;
        };
      }
    endif;
    wp_insert_post($my_post);
  }
}
add_action( 'after_setup_theme', 'insert_page' );
