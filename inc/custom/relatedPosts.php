<?php
function relatedPosts() {
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
		$args=array(
			'tag__in' => $tag_ids,
			'post__not_in' => array($post->ID),
				'posts_per_page'=> 4, // Number of related posts to display.
				'ignore_sticky_posts'=>1
			);
		$my_query = new WP_Query( $args );
		if ($my_query->have_posts()) {
			echo '<aside class="widget_posts">';
		  	echo "<h4 class='related-title'>関連記事</h4><div class='border'></div>";
		  	echo '<ul>';
		}
			while( $my_query->have_posts() ):
				$my_query->the_post();
					echo '<li>';
					if (has_post_thumbnail()):
						echo '<div class="related-img">';
							echo '<a href= "' . get_the_permalink() . '">';
								the_post_thumbnail();
							echo '</a>';
						echo '</div>';
					else:
						echo '<div class="related-img">';
							echo '<a href= "' . get_the_permalink() . '">';
							?>
								<img src="<?php echo get_option('ink_image') ? get_option('ink_image') : bloginfo('template_directory') . '/src/images/thumbnail.png'; ?>" />
							<?php
							echo '</a>';
						echo '</div>';
					endif;
					echo '<div class="related-post-title">';
						echo '<a href= "' . get_the_permalink() . '">';
							the_title();
						echo '</a>';
					echo '</div>';
				echo '</li>';
			endwhile;
			echo '</ul>';
		echo "</aside>";
	}
	wp_reset_query();
}
