<?php
/*
* Popular Post Getter & Setter
*/
function set_popular_post_views($postID) {
  $count_key = 'popular_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count=='') {
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '1', true);
  } else {
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}

function get_popular_post_views($postID) {
  $count_key = 'popular_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count=='') {
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0', true);
    return "0";
  }
  return $count;
}

function get_popular_posts() {
  $popularpost = new WP_Query(
    array(
      'posts_per_page'        => 6,
      'post_status'           => 'publish',
      'post_type'             => 'post',
      'meta_key'              => 'popular_post_views_count',
      'orderby'               => 'meta_value_num',
      'order'                 => 'DESC',
      'ignore_sticky_posts'   => true
    )
  );

  if ($popularpost->have_posts()) :
    echo "<aside class='popular-wrap'>";
      echo "<h4 class='aside-title'>人気の投稿</h4>";
      echo "<ul>";
        while ( $popularpost->have_posts() ) :
          $popularpost->the_post();
          echo '<li>';
            $fill = ' fill';
            if ( has_post_thumbnail( $popularpost->ID ) ) {
              echo '<div class="pop-img">';
                echo '<a href="' . get_permalink( $popularpost->ID ) . '" title="' . esc_attr( $popularpost->post_title ) . '">';
                  echo the_post_thumbnail( 'thumbnail');
                echo '</a>';
              echo '</div>';
              $fill = '';
            }
            echo '<div class="header-overlay' . $fill . '">';
                echo '<a href="' . esc_url( get_permalink() ) . '">';
                  echo '<span class="view-count">' . get_popular_post_views(get_the_id()) . '</span>';
                echo '</a>';
              echo '<h3 class="pop-title">';
                echo '<a href="' . esc_url( get_permalink() ) . '">';
                  the_title();
                echo '</a>';
              echo '</h3>';
            echo '</div>';
          echo '</li>';
        endwhile;
      echo '</ul>';
    echo "</aside>";
  endif;
}
