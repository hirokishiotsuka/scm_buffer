<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package scm_buffer
 */
?>
			</div><!-- .site-content -->
			<?php
				$idclass_post = new WP_Query(
					array(
						'posts_per_page' => 1,
						'post_status' => 'publish',
						'orderby' => 'date',
						'order' => 'DESC',
						'meta_query' => array(
							array(
								'key' => 'colophon',
								'value' => '',
								'compare' => '!='
							)
						)
					)
				);
				$idclass_custom = array();
				if ($idclass_post->have_posts()) :
					while ( $idclass_post->have_posts() ) :
						$idclass_post->the_post();
						$idclass_custom = get_post_custom();
					endwhile;
				endif;
				wp_reset_postdata();
			?>
			<footer id="<?php echo array_key_exists('colophon', $idclass_custom)? $idclass_custom['colophon'][0] : 'colophon'; ?>" class="site-footer" role="contentinfo">
				<div id="pagetop" class="pagetop">
					<?php
					$fontawesome_post = new WP_Query(
						array(
							'posts_per_page' => 1,
							'post_status' => 'publish',
							'orderby' => 'date',
							'order' => 'DESC',
							'meta_query' => array(
								array(
									'key' => 'fa-angle-double-up',
									'value' => '',
									'compare' => '!='
								)
							)
						)
					);
					$fontawesome_custom = array();
					if ($fontawesome_post->have_posts()) :
						while ( $fontawesome_post->have_posts() ) :
							$fontawesome_post->the_post();
							$fontawesome_custom = get_post_custom();
						endwhile;
					endif;
					wp_reset_postdata();
					if (array_key_exists("fa-angle-double-up", $fontawesome_custom)) {
						$fa_pagetop = $fontawesome_custom["fa-angle-double-up"][0];
						if (preg_match("/^fa-/", $fa_pagetop)) {
							$pagetop = '<i class="fa ' . $fa_pagetop . '" aria-hidden="true"></i>';
						} else {
							$pagetop = '<i class="fa">' . $fa_pagetop . '</i>';
						}
					} else {
						$pagetop = '<i class="fa fa-angle-double-up" aria-hidden="true"></i>';
					}
					?>
					<a href="#"><?php echo $pagetop; ?></a>
				</div><!-- #pagetop -->

				<div class="site-info">
					<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('copyright')): ?>
					<?php endif; ?>
				</div><!-- .site-info -->
			</footer><!-- .site-footer -->
		</div><!-- .site -->
		<?php wp_footer(); ?>
	</body>
</html>
