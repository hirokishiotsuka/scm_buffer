/**
 * upload.js
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

(function($) {
  $j = jQuery.noConflict();
  $j(document).ready(function() {
    /* user clicks button on custom field, runs below code that opens new window */
    $j('.camera_wrap').camera({
      height: '42%',
      time: 7000,
      pagination: false,
      thumbnails: true
    });
  });
})(jQuery);
