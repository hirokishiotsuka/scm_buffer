/**
 * font-awesome.js
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

(function($) {
  // Site title and description.
  $('#fa-submit').on('click', function() {
    var faHome = $('#id-fa-home').val(),
        faAngleDoubleUp = $('#id-fa-angle-double-up').val(),
        faFolderOpen = $('#id-fa-folder-open').val(),
        faTags = $('#id-fa-tags').val(),
        faChevronCircleRight = $('#id-fa-chevron-circle-right').val();
    $.ajax({
      type: 'POST',
      url: ajaxurl,
      data: {
        'action' : 'fontawesome_save_postdata',
        'ajax_fa-home': faHome,
        'ajax_fa-angle-double-up': faAngleDoubleUp,
        'ajax_fa-folder-open': faFolderOpen,
        'ajax_fa-tags': faTags,
        'ajax_fa-chevron-circle-right': faChevronCircleRight
      },
      success: function(response) {
      }
    });
    return false;
  });
})(jQuery);
