<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package scm_buffer
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $over_two_line = mb_strlen(get_the_title()) > 56 ? true : false; ?>
	<header class="entry-header<?php if($over_two_line) { echo ' over'; } ?>">
		<?php the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>
	</header><!-- entry-header -->
	<?php
	if ( has_post_thumbnail() ) : ?>
	<div class="entry-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .entry-thumbnail -->
	<?php
	endif;
	?>
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
	<div class="entry-meta">
		<?php scm_buffer_entry_footer(); ?>
		<div class="box">	</div>
	</div><!-- entry-meta -->
</article><!-- #post-## -->
